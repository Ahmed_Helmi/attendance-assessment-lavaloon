import sqlite3
from sqlite3 import Error
from datetime import datetime, timedelta, timezone
import json
import pytz

"""
This is a script solving my assessment.
Note: 
I meant to keep everything in one file so you can easily review the code.

Written by Ahmed Shawkat Helmi
rev 0.1

"""

def create_connection(db_file):
    """ 
    create a database connection to the SQLite database
        attendance.db

    :param db_file: database file
    :return: Connection object or None
    """

    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn



def qs_formatter(cur,rowsQueryset):
    """
    generates a dictionary of queryset tuples
    
    :param cur: database curser
    :param rowsQueryset: results comming from the datatbase as a tuple of tuples
    :return: list of ditcionaries containing results
    """

    headers = list(map(lambda x: x[0], cur.description))
    result=[dict(zip(headers, values)) for values in rowsQueryset]
    return result


def history_formatter(cur, rowsQueryset):
    """
    generates the history of attendances for a particular user

    :param cur: database curser
    :param rowsQueryset: results comming from the datatbase as a list of dicts
    :return: Json dump of the files results
    """
    previousDate=None
    currentDate=None
    finalFormat={"days":[]}
    
    for item in range(len(rowsQueryset)):
        rowsQueryset[item]['ActionTime']=datetime.strptime(
                rowsQueryset[item]['ActionTime'], '%Y-%m-%d %I:%M %p').replace(tzinfo=timezone.utc).isoformat()
        if(item==0):
            currentDate=rowsQueryset[item]['day']
            finalFormat['days'].append(rowsQueryset[item]['day'])
            finalFormat['days'].append(rowsQueryset[item])
            print("new")

        else:
            currentDate = rowsQueryset[item]['day']
            previousDate = rowsQueryset[item-1]['day']
            if(previousDate== currentDate):
                finalFormat['days'].append(rowsQueryset[item])

                print("cntue")
            else:
                finalFormat['days'].append(rowsQueryset[item]['day'])
                finalFormat['days'].append(rowsQueryset[item])
                print("new")
    return json.dumps(finalFormat,indent=3)



def checksIn_and_Out(ListOfDays):
    """
    Counts of check ins and checkouts to help calculating time

    :param ListOfDays: is a list of dicts having a queryset
    :return: returns the number of checkins and checkouts as a tuple
    """

    startTime=[] 
    endTime=[]
    for day in ListOfDays:
        if day["Action"] == "CheckIn":
            startTime.append(datetime.strptime(
                day['ActionTime'], '%Y-%m-%d %I:%M %p'))
        elif day["Action"] == "CheckOut":
            endTime.append(datetime.strptime(
                day['ActionTime'], '%Y-%m-%d %I:%M %p'))
    return startTime,endTime



def normalDays(ListOfDays):
    """
    calculates the difference of the days provided in the case of balanced number of chekins and checkouts

       :param ListOfDays: is a list of dicts having a queryset
       :return: returns the difference in times
    """

    startTime = []
    endTime = []
    startTime,endTime=checksIn_and_Out(ListOfDays)
    total_times=[]
    if(endTime[0].time() > startTime[0].time() and startTime and endTime):
        for time in range(len(startTime)):
            total_times.append(endTime[time]-startTime[time])
            return total_times[time].seconds/60/60                    
    else:
        #when the times are not in order we calculate the difference from 12:00 AM
        for time in range(len(startTime)):
            endTime.insert(0, datetime.strptime(
                ListOfDays[time]['ActionTime'], '%Y-%m-%d %I:%M %p').replace(hour=00,minute=00))
            startTime.append(datetime.strptime(
                ListOfDays[time]['ActionTime'], '%Y-%m-%d %I:%M %p').replace(hour=00, minute=00))
            total_times.append(startTime[time]-endTime[time])
            print(total_times)
        return total_times[0].seconds/60/60


def misplaced_days(ListOfDays):
    """
    calculates the difference of the days provided in the case of imbalance in the number of chekins and checkouts

       :param ListOfDays: is a list of dicts having a queryset
       :return: returns the difference in times
    """
    startTime = []
    endTime = []
    startTime, endTime = checksIn_and_Out(ListOfDays)
    checkIn = len(startTime)
    checkOut= len(endTime)
    if(checkIn>checkOut):
        # I prefer to use 2 while loops in if statement so we can check once instead of twice
        counter=0
        while(checkIn != checkOut):
            ListOfDays.append(
                {"Action": "CheckOut", "ActionTime":  ListOfDays[counter]['ActionTime'][:11]+"12:00 AM"})
            checkOut +=1
            counter +=1
    else:
        counter = 0
        while(checkIn != checkOut):
            ListOfDays.append({"Action": "CheckIn", "ActionTime":  ListOfDays[counter]['ActionTime'][:11]+"12:00 AM"})
            checkIn += 1
            counter += 1
        return normalDays(ListOfDays)
        

def completeDay(ListOfDays):
    """
    arranges the process of calculating how many hours worked

       :param ListOfDays: is a list of dicts having a queryset
       :return: returns the difference in times
    """
    startTime = []
    endTime = []
    startTime, endTime = checksIn_and_Out(ListOfDays)
    checkIns=len(startTime)
    checkOuts=len(endTime)
    if checkIns==checkOuts and checkIns > 0 and checkOuts > 0:
        return normalDays(ListOfDays)
    elif checkIns != checkOuts:
        return misplaced_days(ListOfDays)  


def get_attendance(conn, day,employee):
    """
    gets attecndance for an employee in a particular day
    :param conn: the Connection object
    :param day: date of the day
    :param employee: has employee code as per the datatbase
    """ 
    cur = conn.cursor()
    #Full inner join will create a table as per the parameters
    cur.execute("SELECT action,actionTime,day,employee FROM AttendanceActions INNER JOIN Attendance ON Attendance.Id = AttendanceActions.AttendanceId WHERE day=? AND employee =?", (day,employee,))
    rows = cur.fetchall()
    if(len(rows)):   
        #If attecndance is calculated for the user a dict will be returned as per the following
        print({
            "Attended":True,
            "Duration":str(int(completeDay(qs_formatter(cur, rows))))+" : "+
            str(int(completeDay(qs_formatter(cur, rows))*60%60))})
    else:
        #if there is no attendace record of no users a dict will be returned as per the following

        print({
            "Attended":False
        })    
   
def attendance_history(conn,employee):
    """
    gets attecndance history for an employee

    :param conn: the Connection object
    :param employee: has employee code as per the datatbase
    """

    cur = conn.cursor()
    #Full inner join will create a table as per the parameters
    cur.execute("SELECT action,actionTime,day FROM AttendanceActions INNER JOIN Attendance ON Attendance.Id = AttendanceActions.AttendanceId WHERE employee =? ORDER BY day", (employee,))
    rows = cur.fetchall()
    if(len(rows)):
        print(history_formatter(cur,qs_formatter(cur, rows)))
    else:
        print({})



def main():
    #database directory
    database = r"./attendance.db"

    # create a database connection
    conn = create_connection(database)
    with conn:
        #open connection and try out the following
        get_attendance(conn, "2020-04-03","EMP01")
        attendance_history(conn, "EMP01")


if __name__ == '__main__':
    main()
